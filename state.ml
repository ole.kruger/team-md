open Base

let encode_string s = `String s

let encode_option encode_value o: JSON.u =
  match o with
    | None -> `Null
    | Some x -> encode_value x

let decode_option decode_value json =
  Option.map decode_value (JSON.as_opt json)

let encode_set encode_element get_elements set: JSON.u =
  `A (List.map encode_element (get_elements set))

let decode_set decode_element set_of_list json =
  set_of_list (List.map decode_element (JSON.as_list json))

let encode_map encode_key encode_value get_bindings map: JSON.u =
  `O (List.map (fun (k, v) -> encode_key k, encode_value v) (get_bindings map))

let decode_map decode_key decode_value empty add json =
  let decode_binding (k, v) = decode_key json k, decode_value v in
  let bindings = List.map decode_binding (JSON.as_object json) in
  List.fold_left (fun acc (k, v) -> add k v acc) empty bindings

module ID =
struct
  type project = string (* e.g. "tezos/tezos" *)

  type t =
    | Issue of project * int
    | MR of project * int

  let compare = (Stdlib.compare: t -> t -> int)

  let show = function
    | Issue (project, id) -> sf "%s#%d" project id
    | MR (project, id) -> sf "%s!%d" project id

  let show_short = function
    | Issue ("tezos/tezos", id) -> sf "#%d" id
    | MR ("tezos/tezos", id) -> sf "!%d" id
    | id -> show id

  let show_url = function
    | Issue (project, id) -> sf "https://gitlab.com/%s/-/issues/%d" project id
    | MR (project, id) -> sf "https://gitlab.com/%s/-/merge_requests/%d" project id

  let encode id = `String (show id)

  let issue_rex = rex "^(.+/.+)#(\\d+)$"
  let mr_rex = rex "^(.+/.+)!(\\d+)$"

  let decode_string json s =
    match s =~** issue_rex with
      | Some (project, id) ->
          Issue (project, int_of_string id)
      | None ->
          match s =~** mr_rex with
            | Some (project, id) ->
                MR (project, int_of_string id)
            | None ->
                JSON.error json "invalid ID"

  (* We keep the [json] parameter for better errors. *)
  let decode json = decode_string json (JSON.as_string json)
end

module ID_set = Set.Make (ID)
module ID_map = Map.Make (ID)

module Day =
struct
  type t =
    {
      year: int;
      month: int;
      day: int;
    }

  let compare a b =
    let c = Int.compare a.year b.year in
    if c <> 0 then c else
      let c = Int.compare a.month b.month in
      if c <> 0 then c else
        Int.compare a.day b.day

  let rex = rex "^(\\d{4})-(\\d{2})-(\\d{2})$"

  let show { year; month; day } = sf "%04d-%02d-%02d" year month day

  let encode date = `String (show date)

  let parse s =
    match s =~*** rex with
      | None ->
          None
      | Some (year, month, day) ->
          Some {
            year = int_of_string year;
            month = int_of_string month;
            day = int_of_string day;
          }

  let decode_string json s =
    match parse s with
      | None ->
          JSON.error json "invalid date: %s" s
      | Some day ->
          day
end

module Day_map = Map.Make (Day)

module Topic =
struct
  type status =
    | Open
    | Closed
    | Merged

  let encode_status status: JSON.u =
    match status with
      | Open -> `String "open"
      | Closed -> `String "closed"
      | Merged -> `String "merged"

  let decode_status json =
    match JSON.as_string json with
      | "open" -> Open
      | "closed" -> Closed
      | "merged" -> Merged
      | s -> JSON.error json "invalid status: %s" s

  type t =
    {
      id: ID.t;
      title: string;
      milestone: string option;
      author: string;
      assignees: String_set.t;
      status: status;
      approvals: String_set.t;
    }

  let encode { id; title; milestone; author; assignees; status; approvals }: JSON.u =
    `O [
      "id", ID.encode id;
      "title", `String title;
      "milestone", encode_option encode_string milestone;
      "author", `String author;
      "assignees", encode_set encode_string String_set.elements assignees;
      "status", encode_status status;
      "approvals", encode_set encode_string String_set.elements approvals;
    ]

  let decode json =
    {
      id = JSON.(json |-> "id" |> ID.decode);
      title = JSON.(json |-> "title" |> as_string);
      milestone = decode_option JSON.as_string JSON.(json |-> "milestone");
      author = JSON.(json |-> "author" |> as_string);
      assignees = decode_set JSON.as_string String_set.of_list JSON.(json |-> "assignees");
      status = JSON.(json |-> "status" |> decode_status);
      approvals = decode_set JSON.as_string String_set.of_list JSON.(json |-> "approvals");
    }
end

module Event =
struct
  type t =
    | Created (* topic was unknown and user is author *)
    | Reopened (* topic was closed or merged and is now opened, and user is author *)
    | Closed (* topic was closed while user was author or assigned *)
    | Merged (* topic was merged while user was author or assigned *)
    | Assigned (* user was unassigned and is now assigned *)
    | Unassigned (* user was assigned and is now unassigned *)
    | Approved (* mr was not approved by user and is now approved by user *)
    | Unapproved (* mr was approved by user and is now not approved by user *)

  let compare = (Stdlib.compare: t -> t -> int)

  let encode event: JSON.u =
    match event with
      | Created -> `String "created"
      | Reopened -> `String "reopened"
      | Closed -> `String "closed"
      | Merged -> `String "merged"
      | Assigned -> `String "assigned"
      | Unassigned -> `String "unassigned"
      | Approved -> `String "approved"
      | Unapproved -> `String "unapproved"

  let decode json =
    match JSON.as_string json with
      | "created" -> Created
      | "reopened" -> Reopened
      | "closed" -> Closed
      | "merged" -> Merged
      | "assigned" -> Assigned
      | "unassigned" -> Unassigned
      | "approved" -> Approved
      | "unapproved" -> Unapproved
      | s -> JSON.error json "invalid event: %s" s
end

module Event_set = Set.Make (Event)
module Event_map = Map.Make (Event)

module User_map = Map.Make (String)

module Timeline =
struct
  type by_topic = Event_set.t ID_map.t
  type by_day = by_topic Day_map.t
  type t = by_day User_map.t

  let empty = User_map.empty

  let add day user id event timeline =
    let by_day = User_map.find_opt user timeline |> default Day_map.empty in
    let by_topic = Day_map.find_opt day by_day |> default ID_map.empty in
    let events = ID_map.find_opt id by_topic |> default Event_set.empty in
    let events = Event_set.add event events in
    let by_topic = ID_map.add id events by_topic in
    let by_day = Day_map.add day by_topic by_day in
    User_map.add user by_day timeline

  let encode_event_set: Event_set.t -> JSON.u = encode_set Event.encode Event_set.elements
  let encode_id_map: by_topic -> JSON.u = encode_map ID.show encode_event_set ID_map.bindings
  let encode_day_map: by_day -> JSON.u = encode_map Day.show encode_id_map Day_map.bindings
  let encode: t -> JSON.u = encode_map Fun.id encode_day_map User_map.bindings

  let decode_event_set: JSON.t -> Event_set.t =
    decode_set Event.decode Event_set.of_list
  let decode_id_map: JSON.t -> by_topic =
    decode_map ID.decode_string decode_event_set ID_map.empty ID_map.add
  let decode_day_map: JSON.t -> by_day =
    decode_map Day.decode_string decode_id_map Day_map.empty Day_map.add
  let decode: JSON.t -> t =
    decode_map (fun _ s -> s) decode_day_map User_map.empty User_map.add
end

(* We split open and closed topics so that we can easily get the list of topics
   to get an update for (to check if they have been closed or merged). *)
type t =
  {
    open_topics: Topic.t ID_map.t;
    closed_topics: Topic.t ID_map.t;
    timeline: Timeline.t;
  }

let empty =
  {
    open_topics = ID_map.empty;
    closed_topics = ID_map.empty;
    timeline = Timeline.empty;
  }

let encode { open_topics; closed_topics; timeline }: JSON.u =
  `O [
    "open_topics", encode_map ID.show Topic.encode ID_map.bindings open_topics;
    "closed_topics", encode_map ID.show Topic.encode ID_map.bindings closed_topics;
    "timeline", Timeline.encode timeline;
  ]

let decode json =
  {
    open_topics =
      decode_map ID.decode_string Topic.decode ID_map.empty ID_map.add
        JSON.(json |-> "open_topics");
    closed_topics =
      decode_map ID.decode_string Topic.decode ID_map.empty ID_map.add
        JSON.(json |-> "closed_topics");
    timeline = Timeline.decode JSON.(json |-> "timeline");
  }

let add_event users_to_track day user id event state =
  if not (String_set.mem user users_to_track) then
    state
  else
    let timeline = Timeline.add day user id event state.timeline in
    { state with timeline }

let diff users_to_track day (old_topic: Topic.t) (new_topic: Topic.t) state =
  let add_event kind user = add_event users_to_track day user new_topic.id kind in
  let add_event_for_users kind users = String_set.fold (add_event kind) users in
  let added_assignees = String_set.diff new_topic.assignees old_topic.assignees in
  let state = add_event_for_users Assigned added_assignees state in
  let removed_assignees = String_set.diff old_topic.assignees new_topic.assignees in
  let state = add_event_for_users Unassigned removed_assignees state in
  let added_approvals = String_set.diff new_topic.approvals old_topic.approvals in
  let state = add_event_for_users Approved added_approvals state in
  let removed_approvals = String_set.diff old_topic.approvals new_topic.approvals in
  let state = add_event_for_users Unapproved removed_approvals state in
  match old_topic.status, new_topic.status with
    | Open, Open
    | Closed, Closed
    | Merged, Merged ->
        state
    | _, Open ->
        add_event Reopened new_topic.author state
    | _, Closed ->
        let state = add_event Closed new_topic.author state in
        add_event_for_users Closed new_topic.assignees state
    | _, Merged ->
        let state = add_event Merged new_topic.author state in
        add_event_for_users Merged new_topic.assignees state

let get_topic id state =
  match ID_map.find_opt id state.open_topics with
    | None ->
        ID_map.find_opt id state.closed_topics
    | Some _ as x ->
        x

let add_topic (topic: Topic.t) state =
  let state =
    {
      state with
        open_topics = ID_map.remove topic.id state.open_topics;
        closed_topics = ID_map.remove topic.id state.closed_topics;
    }
  in
  match topic.status with
    | Open ->
        { state with open_topics = ID_map.add topic.id topic state.open_topics }
    | Closed | Merged ->
        { state with closed_topics = ID_map.add topic.id topic state.closed_topics }

let update users_to_track day state (new_topic: Topic.t) =
  let state =
    match get_topic new_topic.id state with
      | None ->
          let state =
            add_event users_to_track day new_topic.author new_topic.id Created state
          in
          diff users_to_track day { new_topic with assignees = String_set.empty }
            new_topic state
      | Some old_topic ->
          diff users_to_track day old_topic new_topic state
  in
  add_topic new_topic state

let update_with_list users_to_track day state topics =
  List.fold_left (update users_to_track day) state topics

let save filename state =
  with_open_out filename @@ fun ch ->
  output_string ch (JSON.encode_u (encode state))

let load filename =
  decode (JSON.parse_file filename)

let clear_timeline state =
  { state with timeline = Timeline.empty }
