open Base
open State

(* let output_event buffer (event: Event.t) count = *)
(*   Buffer.add_string buffer ( *)
(*     match event with *)
(*       | Created -> "  - created" *)
(*       | Reopened -> "  - reopened" *)
(*       | Closed -> "  - closed" *)
(*       | Merged -> "  - merged" *)
(*       | Assigned -> "  - assigned" *)
(*       | Unassigned -> "  - unassigned" *)
(*       | Approved -> "  - approved" *)
(*       | Unapproved -> "  - unapproved" *)
(*   ); *)
(*   if count > 1 then ( *)
(*     Buffer.add_string buffer " (×"; *)
(*     Buffer.add_string buffer (string_of_int count); *)
(*     Buffer.add_string buffer ")"; *)
(*   ); *)
(*   Buffer.add_char buffer '\n' *)

let output_topic state buffer user id events =
  let title, status, assigned, approved, created =
    match State.get_topic id state with
      | None ->
          "???", "", "", "", ""
      | Some topic ->
          let is_open =
            match topic.status with
              | Open -> true
              | Closed | Merged -> false
          in
          let approved = (Event_map.find_opt Approved events |> default 0) > 0 in
          let created = (Event_map.find_opt Created events |> default 0) > 0 in
          topic.title,
          (
            match topic.status with
              | Open -> ""
              | Closed -> " (closed)"
              | Merged -> " (merged)"
          ),
          (if String_set.mem user topic.assignees && is_open then ":a: " else ""),
          (if approved then ":+1: " else ""),
          (if created then ":new: " else "")
  in
  Buffer.add_string buffer
    (sf "- %s%s%s[%s%s](%s) %s\n" assigned approved created (ID.show_short id) status
       (ID.show_url id) title)

let event_count_map_of_set set =
  let add_event event map =
    Event_map.add event ((Event_map.find_opt event map |> default 0) + 1) map
  in
  Event_set.fold add_event set Event_map.empty

let merge_days (by_day: Timeline.by_day): int Event_map.t ID_map.t =
  let merge_day _day (by_topic: Timeline.by_topic) (map: int Event_map.t ID_map.t) =
    let merge_events _id (a: Event_set.t option) (b: int Event_map.t option) =
      let merge_counts _event a b = Some ((a |> default 0) + (b |> default 0)) in
      Some (
        Event_map.merge merge_counts
          (a |> default Event_set.empty |> event_count_map_of_set)
          (b |> default Event_map.empty)
      )
    in
    ID_map.merge merge_events by_topic map
  in
  Day_map.fold merge_day by_day ID_map.empty

let output_user ~from state name_map buffer user by_day =
  let by_day = Day_map.filter (fun day _ -> Day.compare day from >= 0) by_day in
  let merged = merge_days by_day in
  let pretty_name = String_map.find_opt user name_map |> default user in
  Buffer.add_string buffer (sf "\n%s:\n" pretty_name);
  ID_map.iter (output_topic state buffer user) merged

let make_timeline ~from (state: State.t) name_map =
  let buffer = Buffer.create 1024 in
  User_map.iter (output_user ~from state name_map buffer) state.timeline;
  Buffer.contents buffer
